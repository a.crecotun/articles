# Грамматика английского языка

## Past Simple

### Used to

#### used to do (used to + infinitive)
Конструкция used to do используется для описания действий (привычек), которые происходили регулярно в прошлом, но более не происходят

- I used to smoke a packet a day but I stopped two years ago.
- Ben used to travel a lot in his job but now, since his promotion, he doesn’t.
- I used to drive to work but now I take the bus.

#### to be used to doing (to be used to +-ing)
Конструкция *to be used to doing* используется, что бы сказать о чем-то нормальном, обычном для нас, и переводится как “быть привыкшим что-то делать”.

- I’m used to living on my own. I’ve done it for quite a long time.
- Hans has lived in England for over a year so he is used to driving on the left now.
- They’ve always lived in hot countries so they aren’t used to the cold weather here.

#### to get used to doing
Конструкция *to get used to doing* используется для описания процесса привыкания, и переводится как “привыкнуть что-то делать”.

- I didn’t understand the accent when I first moved here but I quickly got used to it.
- She has started working nights and is still getting used to sleeping during the day.
- I have always lived in the country but now I’m beginning to get used to living in the city.

## Present Continuous
Предмет + **am/is/are** + глагол с окончанием **-ing**

| Предмет | Глагол *to be* | Глагол с окончанием *-ing* |
|--|--|--|
| I | am | writing |
| He <br> She <br> It | is | swimming |
| We <br> You <br> They | are | playing |

<!-- | Глагол *to be* | Предмет | Глагол с окончанием *-ing* | Остальные слова |
|--|--|--|
| Am <br> | i | writing | this right? |
| He <br> She <br> It | is | swimming |
| We <br> You <br> They | are | playing | -->

### Случаи
- Для обозначения действий, которые происходят сейчас. Это может происходит прямо в эту секунду, на этой неделе или в этом году. Это что-то продолжительное, что происходит сейчас.
	> I'm reading a book now.  
	> She is living with me right now.

- Для обозначеия действий, которые развиваются в настоящее время:
	> The climate is getting warmer.

- Для обозначения будущих планов:
	> He is working on Monday morning.

- Для выражения раздражения или недовольства с наречиями **always**, **constantly**.
	> You are always coming late.

Слова: now, at the moment, nowadays — ставятся либо в конец, либо в начало предложения.
> Now i am writing code  
> He is playing guitar now.

### Окончания глаголов
**-ie** -> **-y**.
> die - dying
> tie - tying

**c** -> **-ck**
> panic - panicking

**-y**, **-ee**, **-i** -> **-ing**
> study - studying
> see - seeing

## Past Continuous
Употребляется, если:
- Мы говорим о действии, которое продолжалось в определенный момент в прошлом.
	Не важно, когда действие началось или закончилось. Действие именно продолжалось в какой-то определенный момент.
	> I was reading at 6 am

- Мы говорим о длительном действии, которое было прервано другим, более коротким действием.
	В данном случае более продолжительное действие выражено временем **Past Continuous**, а короткое **Past Simple**.
	> When she came a was talking to my friend

- Мы говорим о двух действиях в прошлом, которые происходили одновременно. В этом случае почти всегда можно использовать союз **while** (в то время как).
	> Jane was sleeping while Robert was cooking.

Мы употребляем наречия **always/constantly**, для того чтобы выразить раздражение.
> She was always talking! It was driving my crazy.

| Предмет | Глагол *to be* | Глагол с окончанием *-ing* |
|-|-|-|
| I | was | writing |
| He <br> She <br> It | was | swimming |
| We <br> You <br> They | were | playing |

## Present Perfect Tenses
Используется для того, чтобы сказать:
- о результате: Действие произошло в прошлом, но актуально в настоящем. Результаты действия в прошлом видны и важны в настоящем.
	> I have broken a cup (есть результат действия – осколки)

- о жизненном опыте: О тех действиях, которые когда-то происходили в нашей жизни, о том, что вы когда-либо видели и слышали, и даже о том, что никогда не случалось раньше.
	> I have been to London

- об изменениях: О тех ситуациях и событиях, которые менялись в течение периода времени.
	> My German has really improved since I moved to Germany.

- о достижениях: Любые достижения одного человека или всего человечества.
	> Our son has learned how to read.

- о действиях, которые повторялись несколько раз в прошлом:
	> I have tried to open the door several times.

- о незавершенных действиях в прошлом:
	> My brother hasn't washed all the plates yet.

	Мой брат начал мыть тарелки в прошлом и продолжает их мыть в настоящем. Действие пока не закончилось.

**Present Perfect** используется с **just**, но не используется с **just now**. С **just now** используется **Past Simple**.
> Could you tell me what happened just now?

Present Perfect может использоваться с наречиями, которые указываеют на то, что период времени, когда произошло действие, еще не закончился (today, this year, this month):
> Well, I don't know. I haven't seen her all this week.

Если в предложении есть слова

| слово | перевод |
|-|-|
| already | уже |
| ever | когда-либо |
| never | никогда |
| lately | в последнее время |
| recently | в последнее время |
| yet | еще |
| since | с |
| for | в течение |
| so far | пока |

то часто используется **Present Perfect**

**Present Perfect** не используется с выражениями, которые обозначают точное время совершения действия (вчера, на прошлой неделе, сейчас, в данный момент и т.д.).
Для **Present Perfect** характерны только те выражения, которые указывают на то, что период времени, когда произошло действие, еще не закончился.
К таким временным выражениям относятся:
- already
- before
- ever
- never
- lately
- yet
- since
- for
- so far

## Предлоги
in — Место - регион, город, страна или мир в целом
at — Нахождение вблизи указанного места или объекта
on — Поверхность, на которой кто-либо или что-либо находится
