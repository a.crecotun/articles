# Backbone.js: рекомендации

## Оглавление
- [Модуль](#module)
	- [Файловая структура](#module_file_structure)
	- [Пространство имен](#module_namespace)
	- [App](#module_app)
	- [Инициализация модуля](#module_initialization)
	- [View](#module_view)
		- [initialize](#module_view_initialize)
		- [initEventsListeners](#module_view_initeventlisteners)
		- [initStickitBindings](#module_view_initstickit)
		- [render](#module_view_render)
		- [cacheDOM](#module_view_cache)
		- [attachDOMEvents](#module_view_attach)
	- [Model / Collection](#module_data)
		- [Предустановленные данные](#module_data_prefetched)
- [Миксины](#mixins)
	- [Common](#mixins_common)
	- [Views](#mixins_view)
		- [subViews](#mixins_view_sub)
		- [addValidation](#mixins_view_addvalidation)
		- [close](#mixins_view_close)
		- [removeSubView](#mixins_view_removesubview)
		- [showErrors / clearErrors](#mixins_view_errors)
- [Улучшения и советы](#tips)

## Модуль <a id="module"></a>

### Файловая структура <a id="module_file_structure"></a>

Я рекомендую следующую структуру для одного модуля
```
views/
	base.coffee

models/
	base.coffee

collections/
	items.coffee

router/
	router.coffee

templates/
	base.html

module_name.coffee
```

1. `module_name.coffee` — основной файл вызова модуля, который возвращает функцию-конструктор
2. `views/base.coffee` — основная вьюшка, внутри которой инициализируются подмодули
3. `models/base.coffee` — основная модель. Нужна, чтобы получить данные с сервера и передать подмодулям данные и сохранять данные всех подмодулей.

[Шаблон файла модуля](https://gist.github.com/Krekotun/d4f70e40dba0048ba83a)

### Пространство имен <a id="module_namespace"></a>
Файлы модуля находятся в одном пространстве имен.

Этот объект хранится в `window`, так легче брать нужные части из любого места в приложении (например, компиляция шаблона `window.ModuleName.Templates.Base()`).

```coffeescript
window.ModuleName = ModuleName =
	Views:
		Base: BaseView
	Models:
		Base: BaseModel
	Collections: {}
	Templates:
		Base: Handlebars.compile BaseTmpl

```
Я считаю это анти-паттерном, но другой вариант в requirejs я не пробовал.

### App <a id="module_app"></a>
App - функция-конструктор, запускающая модуль (инициализирует основную вью и данные).  
Возвращает вьюшку/модель/коллекцию и метод `close`.

```coffeescript
class ModuleName.App
	constructor: ->
		@model = new ModuleName.Models.Base()
		@view = new ModuleName.Views.Base
			model: @model

		return {
			# data
			model: @model
			view: @view

			# methods
			close: @close
		}

	close: ->
		@view.close?()
		@view.remove?()
```


### Инициализация <a id="module_initialization"></a>
Настоятельно рекомендую инициализировать и добавлять модуль на страницу следующий способом.

```coffeescript
module = new ModuleName.App()
$( module.view.render().el ).appendTo( @$el )
```
Метод `render` внутри модуля не должен добавлять html-код куда-либо на страницу самостоятельно.


### View <a id="module_view"></a>

#### initialize <a id="module_view_initialize"></a>

Самый минимальный набор для `initialize` метода.
```coffeescript
initialize: (options) ->
	Cocktail.mixin( @, Mixins.Views.Base, Mixins.Common ) # add Mixins

	@setOptions(options)
	@initEventsListeners()
	@initStickitBindings()
	@addValidation()
```
1. Подключили миксины
2. Начали слушать события
3. Привязали модель
4. Добавили валидацию

Старайтесь не вызывать `render` при инициализации вьюшки, а только в месте инициализации модуля.

#### initEventsListeners / addEventListeners / addListeners <a id="module_view_initeventlisteners"></a>
Метод для инициализации прослушивания событий.
Раньше я использовал названия `addEventListeners` и `addListeners`, но `initEventsListeners` подходит больше.
```coffeescript
initEventsListeners: ->
	@listenTo @, 'rendered', =>
		@cacheDOM()
		@attachDOMEvents()
		@stickit()
```

#### initStickitBindings / addStickit <a id="module_view_initstickit"></a>
Раньше использовал `addStickit`, рекомендую `initStickitBindings`.
Внутри только объект `@bindings`

```coffeescript
initStickitBindings: ->
	@bindings =
		'[name="title"]': 'title'

	@
```

#### render <a id="module_view_render"></a>
Внутри метода `render` происходит только рендер шаблона `@$el` и вызов события о завершении.  
Так вьюшка узнает, что ей можно связывать модель и кешировать DOM.  

**Хорошо**
```coffeescript
render: ->
	@$el.html ModuleName.Templates.Base()
	@trigger 'rendered'

	@
```

**Плохо**  
Метод вызывает побочные методы, что усложняет чтение кода.
```coffeescript
render: ->
	@$el.html ModuleName.Templates.Base(response.data)
	@stickit()
	@cacheDOM()

	@
```
**Плохо**  
Запросы к серверу нужно делать внутри модели или коллекции и потом вызывать рендеринг или перерендеринг.
```coffeescript
render: ->
	$.when( @getData() )
		.then
			(response) =>
				@$el.html ModuleName.Templates.Base(response.data)
				@stickit()
				@cacheDOM()

	@
```

#### cacheDOM <a id="module_view_cache"></a>
Кэшируем jquery-объекты в переменные для быстрого доступа.  
Я использую `@$el` в качестве пространсва имен (например, `@$el.$list`).

```coffeescript
cacheDOM: ->
	@$el.$list = @$('.module--list')
	@$el.$button = @$('module--button')

	@
```

#### attachDOMEvents <a id="module_view_attach"></a>
Разделяем события приложения и события DOM.

```coffeescript
attachDOMEvents: ->
	@$el.$button.on "click", (e) =>
		console.log e.target
```

### Model / Collection <a id="module_data"></a>
Основные методы работы с данными (`fetch`, `save` и тд.) я переписывал. С самого начала бекенд не был заточен под архитектуру бэкбона, но сейчас ситуация стала лучше и я бы рекомендовал использовать подход бэкбона.
Вместо стандартного метода `fetch` я использовал свой метод `getData`. Cейчас я считаю, что лучше переписывать поведение `fetch`, так в любой момент можно будет отказаться от своей реализации в пользу нативной без замены названия.

#### Предустановленные данные <a id="module_data_prefetched"></a>
Часто, внутри большого модуля есть подмодули. Каждый подмодуль независим от родителя и имеет свои методы получения данных с сервера. Чтобы избежать большого количества запросов, я рекомендую делать 1 запрос у модуля родителя и передавать данные в подмодули.

**Иницализация и передача данных**
```coffeescript
module = new Module.App({
	data: @model.toJSON()
	})
```

Потом эти данные передаем в модель и уже там вызываем `fetch` с ними.
```coffeescript
# переписанный метод fetch, который вызывается при инициализации модели/коллекции
fetch: (data) ->
	if data?
		# записываем данные в модель или коллекцию
	else
		# делаем аякс запрос или вызываем стандартный fetch
```

## Миксины <a id="mixins"></a>
От модуля к модулю используются одни и те же функции: удалить вьюшку с подвьюшками, сделать опции "глобальными", инициализировать валидацию и тд. Чтобы все это не копировать от файла к файлу, нужно использовать миксины. Миксин — класс/объект с набором методов, которые добавляются в модуль для расширения его возможостей.

Для работы с миксинами в Backbone я использую плагин [Cocktail](https://github.com/onsi/cocktail).
В использовании он до безобразия прост:
```coffeescript
Cocktail.mixin(@, Mixins.Common, Mixins.Views.Base)
```

[Мой миксин](https://gist.github.com/Krekotun/39b7378d0f8d2143e382)

Я разбил файл на несколько частей: `Common`, `Model`, `View`. Нет смысла подключать в `View` методы для `Model` и наоборот. Общий набор методов я вынес в `Common`.

### Common <a id="mixins_common"></a>
Сейчас в `common` у меня только 1 метод `setOptions`. Нужен для того, чтобы все опции, который были переданы в объекте стали доступны через `this`  
Инициализирую я его в самом начале модуля, в `initialize`.

Пример набора опций:
```javascript
options = {
	model: new Backbone.Model(),
	parentView: parentView
}
```
После `@setOptions(options)` мы получим `@model` и `@parentView`.


### Views <a id="mixins_view"></a>
`Views` по идее должен делится на несколько подкатегорий, сейчас это только `Base` — обязательный набор любой вью.

#### subViews <a id="mixins_view_sub"></a>
Массив в который добавляем все подвью, нужен для того, чтобы в будущем легко убрать все сущности.

#### addValidation <a id="mixins_view_addvalidation"></a>
Метод, добавляющий валидацию во вьюшку.
Для валидации я использую плагин [Backbone.Validation](https://github.com/thedersen/backbone.validation)

Метод принимает объект `options`, в котором можно передать:

1. `selector` - для поиска элемента по заданному аттрибуту.
	По умолчанию это `name` ( `$('name="model_field"')` ).  
	Например, `selector: 'data-selector'` будет искать так `$('data-selector="model_field"')`.  
	Поиск элемента расчитан максимум на 1 вложенность `'one.two'` = `name="one[two]"`

2. `valid`/`invalid` — переписывают поведение стандартных методов для поиска и вывода ошибок.

#### close <a id="mixins_view_close"></a>
Метод, который отвечает за удаление подвьюшек в массиве `subViews` (для поддержки старого кода он обходит массив `instances`), удаление текущей вью и модели.

#### removeSubView <a id="mixins_view_removesubview"></a>
Добавляет возможность удалить конкретную вью из определенного массива.

#### showErrors / clearErrors <a id="mixins_view_errors"></a>
Методы для показа/чистки ошибок серверной валидации.
`showErrors` работает также как backbone.validation. В аргументах принимает `errors` и `options`. `options` используется только для задания кастомного селектора как и в `addValidation`.

## Улучшения и советы <a id="tips"></a>
1. Я бы разобрался с [рекурсивными зависимостями в require.js](http://stackoverflow.com/questions/4881059/how-to-handle-circular-dependencies-with-requirejs-amd). Это позволит избавиться от захламления `window` модулями.
2. Подстроить бекенд под бэкбон и пользоваться родными методами на полную катушку
3. Улучшил архитектуру приложения с применением техник и паттернов из [книги Эдди Османи](https://addyosmani.com/largescalejavascript/)
4. [Unit-тестирование](https://addyosmani.com/backbone-fundamentals/#unit-testing)
5. Перечитал бы [книгу](https://addyosmani.com/backbone-fundamentals) внимательнее и до конца, многие вещи я придумывал сам или вычитывал откуда-то
6. Не забывать про [Backbone zombie views](https://www.google.com/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=zombie%20view%20backbone)
